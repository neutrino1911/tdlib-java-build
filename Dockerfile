FROM openjdk:11-jre-slim-buster

COPY libtdjni.so /usr/lib/libtdjni.so

RUN apt-get update && apt-get install -y --no-install-recommends libc++1 && rm -rf /var/lib/apt/lists/*
